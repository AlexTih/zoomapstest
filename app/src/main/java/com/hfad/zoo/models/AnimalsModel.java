package com.hfad.zoo.models;

import java.io.Serializable;

/**
 * Created by александр on 09.08.2017.
 */

public class AnimalsModel implements Serializable {
    private String name;
    private String description;
    private String url;
    private int type;
    private double lat;
    private double lng;

    public AnimalsModel(String name, String description, String url, int type, double lat, double lng){
        this.name = name;
        this.description = description;
        this.url = url;
        this.type = type;
        this.lat = lat;
        this.lng = lng;
    }

    public String getName(){return name;}
    public String getDescription(){return description;}
    public String getUrl(){return url;}
    public int getType(){return type;}
    public double getLat(){return lat;}
    public double getLng(){return lng;}
}
