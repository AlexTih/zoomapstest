package com.hfad.zoo.providers;

import android.content.Context;
import android.content.res.AssetManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hfad.zoo.models.AnimalsModel;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by александр on 09.08.2017.
 */

public class AnimalsModelProvider {
    public static ArrayList<AnimalsModel> providerAnimalModels(Context context) throws IOException{
        ArrayList<AnimalsModel> items = new ArrayList<>();
        Gson gson = new Gson();
        Type type = new TypeToken<List<AnimalsModel>>() {}.getType();
        items = gson.fromJson(getStringFromAssetFile(context),type);
        return items;
    }

    public static String getStringFromAssetFile(Context context) throws IOException {
        AssetManager am = context.getAssets();
        InputStream is = am.open("animals.json");
        String s = convertStreamToString(is);
        is.close();
        return s;
    }
    private static String convertStreamToString(InputStream is) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int i = is.read();
        while (i != -1) {
            baos.write(i);
            i = is.read();
        }
        return baos.toString();
    }
}
